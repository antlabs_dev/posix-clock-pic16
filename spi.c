/**
 * @file 	spi.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * SPI wrapper for loading data to 74164
 */

#include <htc.h>

#include "spi.h"

/**
 * Send a byte over SPI
 *
 * @param byte - byte to send
 */
static void spi_byte_write(uint8_t byte);

void spi_write_uint32(uint32_t data)
{
	spi_reg_clear();
	spi_byte_write(data >> 24u);
	spi_byte_write(data >> 16u);
	spi_byte_write(data >> 8u);
	spi_byte_write(data >> 0u);
}

void spi_init(void)
{
	/* SPI pins */
	TRISC3 = 0;
	TRISC5 = 0;
	/* Clear pin set to high */
	TRISC2 = 0;
	RC2 = 1u;
	/* Transmit on rising edge, last byte addr */
	SSPSTAT = 0b01000000;
	/* No collisions, no overflow, enable peripheral, clock idle high, master Fosc/4*/
	SSPCON = 0b00110000;
}

void spi_reg_clear(void)
{
	RC2 = 0;
	RC2 = 1u;
}

void spi_enable(void)
{
	SSPEN = 1u;
	TRISC3 = 0;
	TRISC5 = 0;
	TRISC2 = 0;
}

void spi_disable(void)
{
	SSPEN = 0;
	TRISC3 = 1u;
	TRISC5 = 1u;
	TRISC2 = 1u;
}

static void spi_byte_write(uint8_t byte)
{
	SSPBUF = byte;
	 /* wait for transmission complete */
	while(!SSPIF);
}
