/**
 * @file 	rtc.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Real TIme Clock with Timer1
 */

#include <htc.h>

#include "rtc.h"

/**
 * Time counter
 */
static volatile uint32_t thetime = 0;

void rtc_init(void)
{
	/* Timer1 1:1 prescale, use oscillator, do not sync, start */
	T1CON = 0b00001111;
	/* Preload value */
	TMR1H = 0x80;
	/* Enable Timer1 interrupt*/
	TMR1IE = 1u;
}

uint32_t rtc_get_posix_time(void)
{
	return thetime;
}

void rtc_set_posix_time(uint32_t newtime)
{
	thetime = newtime;
}

uint_fast8_t rtc_isr(void)
{
	if(TMR1IF)
	{	/* Timer1 interrupt. Reload timer and increment counter */
		TMR1IF = 0;
		TMR1H = 0x80;
		thetime++;
		return 1u;
	}
	return 0;
}
