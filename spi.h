/**
 * @file 	spi.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * SPI wrapper for loading data to 74164
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>

/**
 * Initialize SPI module
 */
void spi_init(void);

/**
 * Transmit 32 bit integer via SPI
 */
void spi_write_uint32(uint32_t data);

/**
 * Clear register latches
 */
void spi_reg_clear(void);

/**
 * Enable SPI (for sleep)
 */
void spi_enable(void);

/**
 * Disable SPI (for sleep)
 */
void spi_disable(void);

#endif /* SPI_H_ */
