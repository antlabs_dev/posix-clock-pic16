/**
 * @file 	adc.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * ADC wrapper module
 */

#include <htc.h>

#include "adc.h"

void adc_start(void)
{
	/* Enable ADC, select RA0/AN0 channel, Fosc/32 clock */
	ADCON0 = 0b10000001;
	ADCON1 = 0b00000100;
	GO_DONE = 1u;
	ADIE = 1u;
}

uint_fast8_t adc_isr(void)
{
	if(ADIF)
	{
		ADIF = 0;
		if(ADRES < 45u)
		{	/* Threshold reached */
			return 1u;
		}
		adc_start();
	}
	return 0;
}

void adc_disable(void)
{
	ADON = 0;
	ADIE = 0;
}
