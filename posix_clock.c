/**
 * @file 	posix_clock.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Main module
 */

#include <htc.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "uart.h"
#include "rtc.h"
#include "spi.h"
#include "cli.h"
#include "adc.h"

/**
 * Configuration bits
 */
__CONFIG(WDTE_OFF & FOSC_HS & CP_OFF & PWRTE_ON & BOREN_OFF);
__IDLOC(DEAD);

/**
 * Define to 1 to print current time to uart every second
 */
#define PRINTF_EVERY_SEC 0

/**
 * Interrupt state struct. Set inside ISR, checked and cleared in main cycle
 */
static volatile struct s_interrupt_occured {
	uint_fast8_t rtc;	/** RTC interrupt every 1 second */
	uint8_t uart; 		/** Received character via UART */
	uint_fast8_t adc;	/** ADC threshold reached, should go to sleep*/
} event_occured = {0, 0, 0};

/**
 * Entry point
 */
void main(void)
{
	/* Init all peripheral modules */
	rtc_init();
	uart_init();
	spi_init();
	/* Enable peripheral and global interrupts */
	PEIE = 1u;
	GIE = 1u;

	printf("start\n");

	/* First time start ADC*/
	adc_start();

	/* Main loop*/
	while(1)
	{
		if(event_occured.rtc)
		{
			/* Every second reload shift registers */
			spi_write_uint32(rtc_get_posix_time());
#if PRINTF_EVERY_SEC == 1
			printf("%lu\n", rtc_get_posix_time());
#endif
			event_occured.rtc = 0;
		}
		if(event_occured.uart)
		{
			process_cli(event_occured.uart);
			event_occured.uart = 0;
		}
		/* Go to sleep if external power disappear */
		if(event_occured.adc)
		{
			adc_disable();
			uart_disable();
			spi_disable();
			SLEEP();
			spi_enable();
			spi_reg_clear();
			uart_enable();
			adc_start();
			/* Ignore all other events */
			event_occured.adc = 0;
			event_occured.rtc = 0;
			event_occured.uart = 0;

			printf("wake up\n");
		}
	}
}

/**
 * Interrupt service routine
 */
void interrupt isr(void)
{
	event_occured.rtc = rtc_isr();
	event_occured.uart = uart_isr();
	event_occured.adc = adc_isr();
}

