PICC=/mnt/video/hitech/picc-16/bin/picc
CHIP=16F73
SOURCES=posix_clock.c uart.c rtc.c spi.c cli.c adc.c
HEX=posix_clock.hex

all: 
	$(PICC) --CHIP=$(CHIP) $(SOURCES)

clean: 
	rm -fr *.pro *.as *.lst *.obj *.rlf *.cof *.hex *.hxl *.lst *.p1 *.pre *.sdb *.sym funclist
	@echo Done!

burn:
	op -d $(CHIP) -w ./$(HEX) -ee

burnpgm:
	picpgm -p ./$(HEX) 

asm:
	$(PICC) --CHIP=$(CHIP) $(SOURCES) -S

