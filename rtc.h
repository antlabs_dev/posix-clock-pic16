/**
 * @file 	rtc.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Real TIme Clock with Timer1
 */

#ifndef RTC_H_
#define RTC_H_

#include <stdint.h>

/**
 * Initialize RTC module
 */
void rtc_init(void);

/**
 * ISR for RTC. Called from each interrupt
 *
 * @return 1 if there was rtc interrupt, 0 otherwise
 */
uint_fast8_t rtc_isr(void);

/**
 * Get current POSIX time
 *
 * @return number of seconds since epoch
 */
uint32_t rtc_get_posix_time(void);

/**
 * Set current POSIX time
 *
 * @param newtime - current time
 */
void rtc_set_posix_time(uint32_t newtime);

#endif /* RTC_H_ */
