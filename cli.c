/**
 * @file 	spi.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Trivial command line interface
 */

#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

#include "cli.h"
#include "rtc.h"

/**
 * Ring buffer for received data
 */
static volatile struct svs_buffer {
	uint8_t data[16];
	uint8_t ptr;
} buffer;

/**
 * Clear the input buffer
 */
static void clear_buffer(void);

/**
 * Parse received line and call appropriate functions
 */
static void parse(void);

void process_cli(uint8_t byte)
{
	buffer.data[buffer.ptr++] = byte;
	if(byte == '\n' || byte == '\r')
	{
		parse();
		clear_buffer();
	}
	if(buffer.ptr >= sizeof buffer.data)
	{
		clear_buffer();
	}
	if(!isalpha(buffer.data[0]))
	{	/* Discard non-character data*/
		clear_buffer();
	}
}

static void clear_buffer(void)
{
	uint_fast8_t i = 0;
	for(i = 0; i < sizeof buffer.data;  i++)
	{
		buffer.data[i] = 0;
	}
	buffer.ptr = 0;
}

static void parse(void)
{
	/* Available commands */
	const char * const get = "get";
	const char * const set = "set";

	if(memcmp(buffer.data, set, strlen(set)) == 0)
	{
		uint32_t input = strtoul((const char *)&buffer.data[strlen(set) + 1], NULL, 10);
		rtc_set_posix_time(input);
		printf("time set to \"%lu\"\n", input);
	}
	else if(memcmp(buffer.data, get, strlen(set)) == 0)
	{
		uint32_t thetime = rtc_get_posix_time();
		printf("the time: \"%lu\",  %s\n", thetime, ctime((const time_t *)&thetime));
	}
	else
	{
		printf("unknown command %s\n", buffer.data);
		printf("available:\n");
		printf("set [new_posix_time]\n");
		printf("get\n");
	}
}
